package imp.org

import imp.sqlite.Database
import imp.sqlite.cache.DbCache1Only
import imp.sqlite.main.Sqlite
import imp.util.closeOnFail
import imp.util.impCreateParentDirs
import kotlinx.coroutines.runBlocking


val orgDbc = DbCache1Only(::openDb, ttlMs = 30_000)

private var initialized = false

private fun openDb(): Database {
	val file = AppPaths.orgDb
	if(!initialized)
		file.impCreateParentDirs()
	return Sqlite.file(file).closeOnFail { db ->
		if(!initialized) {
			orgOrm.applySchema(db)
			initialized = true
		}
	}
}



@Deprecated("")
object OrgDb {
	suspend fun connectSus(): Database = orgDbc.borrow()
	fun connect(): Database = runBlocking { connectSus() }
}

