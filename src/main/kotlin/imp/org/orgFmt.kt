package imp.org

import imp.org.cui.models.UnitI
import imp.util.fmt.Millis
import imp.util.fmt.TOD12
import imp.util.fmt.TODFmt
import imp.util.fmt.str
import imp.util.pow
import java.time.Duration
import java.time.LocalDateTime
import kotlin.math.absoluteValue

/**Moves the decimal point based on fractionalDigits, and also adds the unit (short) name or symbol.*/
fun UnitI.fmt(amount: Long): String {
	val out = fmtN(amount)
	if(symbol != null)
		return "$symbol$out"
	return out + ' ' + (short ?: name)
}

/**Formats the number but doesn't add any names/symbols. Moves the decimal point based on fractionalDigits.*/
fun UnitI.fmtN(amount: Long): String = fmtFracD(amount, fractionalDigits)

/**Formats the number, shifting the decimal point according to fractionalDigits.*/
fun fmtFracD(amount: Long, fractionalDigits: Int): String {
	val a = amount.absoluteValue
	if(fractionalDigits == 0) {
		return amount.toString()
	} else if(fractionalDigits > 0) {
		val d = pow(10L, fractionalDigits.toLong())
		val left = a / d
		val right = (a % d).toString().padStart(fractionalDigits, '0')
		return "${if(amount < 0) "-" else ""}$left.$right"
	} else {
		val d = pow(10L, -fractionalDigits.toLong())
		return (amount * d).toString()
	}
}




val todFmt: TODFmt = TOD12


/**A user-friendly format that includes more info for times farther from now.*/
fun fmtFromNow(time: LocalDateTime, now: LocalDateTime = LocalDateTime.now()): String {
	if(time.year == now.year && time.dayOfYear == now.dayOfYear)
		return todFmt.dateHm(time)
	val sepMs = Duration.between(time, now).toMillis()
	if(sepMs > -3 * Millis.DAY && sepMs < 2 * Millis.DAY)
		return "${time.dayOfWeek.str()} ${todFmt.hm(time)}"
	return todFmt.dateHm(time)
}