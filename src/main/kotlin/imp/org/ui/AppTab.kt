package imp.org.ui

import javafx.beans.property.ReadOnlyProperty
import javafx.scene.Node

interface AppTab {
	/**Initially null; a MainWindow will set this when the tab is added to the window.*/
	var tabsPanel: TabsPanel?
	val title: ReadOnlyProperty<String>
	val content: Node
	/**What object this tab represents (e.g. a label), or null if this tab doesn't represent anything (e.g. a menu).
	 * This value is used to figure out whether we already have a tab with a particular object (e.g. label).*/
	val represents: Any?

	/**Called when this tab closes. If this throws an exception, the tab will reopen - do this when there is unsaved data the user must fix.*/
	fun onClose() {}
}