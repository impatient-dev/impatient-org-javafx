package imp.org.ui

import javafx.beans.property.StringProperty
import javafx.event.EventHandler
import javafx.scene.Node
import javafx.scene.control.Button
import javafx.scene.input.KeyCode
import javafx.scene.input.MouseButton


/**Manages a button that, when activated, takes the user to a tab. If some existing tab matches the finder, that tab will be selected;
 * otherwise, a new tab will be added. The current tab may or may not be removed, depending on what mouse button (if any) is used and what keys are held down.*/
class AppNavBtn (
	initialText: String,
	private val tab: AppTab,
	private val existingFinder: (AppTab) -> Boolean,
	private val factory: () -> AppTab,
) {
	constructor(tab: AppTab, existingFinder: (AppTab) -> Boolean, factory: () -> AppTab) : this("", tab, existingFinder, factory)

	private val btn = Button(initialText)
	val root: Node get() = btn
	val textProperty: StringProperty get() = btn.textProperty()

	init {
		btn.onMouseClicked = EventHandler { event ->
			when(event.button) {
				MouseButton.PRIMARY -> go(false)
				MouseButton.MIDDLE -> go(true)
				else -> {}
			}
		}
		btn.onKeyPressed = EventHandler { event ->
			if(event.code == KeyCode.ENTER)
				go(false)
		}
	}

	private fun go(forceNewTab: Boolean) {
		val tabsPanel = tab.tabsPanel!!
		val keepCurrentTab = forceNewTab || tabsPanel.window.openLinksInNewTab.get()
		val currentIdx = tabsPanel.indexOf(tab)
		val foundIdx = tabsPanel.indexOf(existingFinder)

		if(foundIdx == -1) {
			if(keepCurrentTab)
				tabsPanel.add(factory(), false)
			else
				tabsPanel.replace(currentIdx, factory())
		} else {
			tabsPanel.select(foundIdx)
			if(!keepCurrentTab)
				tabsPanel.remove(currentIdx)
		}
	}
}