package imp.org.ui.tabs

import imp.jfx.jfxBind
import imp.jfx.withAction
import imp.jfx.withPrompt
import imp.org.OrgLabel
import imp.org.orgExecBackgroundDb
import imp.org.repo.LabelRepo
import imp.org.ui.AppTab
import imp.org.ui.TabsPanel
import imp.org.ui.dialogs.DeleteLabelDialog
import imp.util.logger
import javafx.beans.property.SimpleStringProperty
import javafx.event.EventHandler
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.control.TextArea
import javafx.scene.control.TextField
import javafx.scene.layout.GridPane
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox

private val log = LabelEditTab::class.logger

/**Lets the user edit a label.*/
class LabelEditTab (
	val label: OrgLabel,
) : AppTab {
	private val vTitle = Label("Label")
	private val vDelete = Button("Delete").withAction(EventHandler {openDeleteDialog()})
	private val vTopRow = HBox(vTitle, vDelete)

	private val vNameLabel = Label("Name")
	private val vNameEntry = TextField().withPrompt("name")

	private val vDesriptionLabel = Label("Description")
	private val vDescriptionEntry = TextArea().withPrompt("description").also { it.prefRowCount = 3 }

	private val vEntryGrid = GridPane()
	override val content = VBox(vTopRow, vEntryGrid)

	override val represents = label
	override val title = SimpleStringProperty()
	override var tabsPanel: TabsPanel? = null

	private var delete = false


	init {
		log.debug("Creating {}.", this)
		vNameEntry.text = label.data.name
		vDescriptionEntry.text = label.data.description

		var r = 0
		vEntryGrid.addRow(r++, vNameLabel, vNameEntry)
		vEntryGrid.addRow(r++, vDesriptionLabel, vDescriptionEntry)

		title.jfxBind(vNameEntry.textProperty())
	}


	private fun openDeleteDialog() {
		DeleteLabelDialog(tabsPanel!!.window.stage, label) { delete = true; tabsPanel!!.remove(this) }
	}


	override fun onClose() {
		label.data.name = vNameEntry.text
		label.data.description = vDescriptionEntry.text
		val copy = label.deepCopy()
		orgExecBackgroundDb { db ->
			if (!delete)
				LabelRepo.save(copy, db)
		}
	}

	override fun toString() = "LabelEditTab($label)"
}