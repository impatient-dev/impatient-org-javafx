package imp.org.ui.tabs

import imp.jfx.addListenerAndCallImmediately
import imp.jfx.jfxBind
import imp.jfx.withCellFactory
import imp.jfx.withPrompt
import imp.org.OrgLabel
import imp.org.repo.LabelRepo
import imp.org.ui.AppNavBtn
import imp.org.ui.AppTab
import imp.org.ui.TabsPanel
import javafx.beans.property.SimpleStringProperty
import javafx.beans.value.ChangeListener
import javafx.collections.FXCollections
import javafx.scene.control.Label
import javafx.scene.control.ListCell
import javafx.scene.control.ListView
import javafx.scene.control.TextField
import javafx.scene.layout.VBox

class LabelSearchTab : AppTab {
	val results = FXCollections.observableArrayList<OrgLabel>()

	private val vTitle = Label()
	private val vSearch = TextField().withPrompt("filter")
	private val vList = ListView(results).withCellFactory {MyListCell()}
	private val root = VBox(vTitle, vSearch, vList)

	override var tabsPanel: TabsPanel? = null
	override val title = SimpleStringProperty("Labels")
	override val content = root
	override val represents: Any? = null

	init {
		vTitle.textProperty().jfxBind(results) {"Labels (${results.size})"}
		vSearch.textProperty().addListenerAndCallImmediately(ChangeListener {_, _, search ->
			results.setAll(LabelRepo.getAll().filter {search.isEmpty() || it.data.name.contains(search)})
		})
	}


	private inner class MyListCell : ListCell<OrgLabel>() {
		private val btn = AppNavBtn(this@LabelSearchTab, {it is LabelEditTab && it.label.data.id == item.data.id}, {LabelEditTab(item)})

		init {
			btn.textProperty.jfxBind(this.itemProperty()) {it?.data?.name}
		}

		override fun updateItem(item: OrgLabel?, empty: Boolean) {
			super.updateItem(item, empty)
			this.graphic = if(empty) null else btn.root
		}
	}
}


