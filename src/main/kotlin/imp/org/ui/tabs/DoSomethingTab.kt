package imp.org.ui.tabs

import imp.org.OrgLabel
import imp.org.ui.AppNavBtn
import imp.org.ui.AppTab
import imp.org.ui.TabsPanel
import javafx.beans.property.SimpleStringProperty
import javafx.scene.Node
import javafx.scene.layout.GridPane

/**Menu tab that lets the user choose what to do.*/
class DoSomethingTab : AppTab {
	override var tabsPanel: TabsPanel? = null
	override val title = SimpleStringProperty("New Tab")
	override val represents: Any? = null

	private val btnLabelNew = AppNavBtn("New Label", this, {false}, {LabelEditTab(OrgLabel.newUnsaved())})
	private val btnLabelSearch = AppNavBtn("View Labels", this, {false}, {LabelSearchTab()})

	private val vGrid = GridPane()
	override val content: Node = vGrid

	init {
		var r = 0
		vGrid.addRow(r++, btnLabelSearch.root, btnLabelNew.root)
	}
}