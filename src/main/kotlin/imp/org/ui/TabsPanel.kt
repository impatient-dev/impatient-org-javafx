package imp.org.ui

import imp.jfx.addListenerAndCallImmediately
import imp.jfx.jfxBind
import imp.util.logger
import javafx.scene.Node
import javafx.scene.control.Tab
import javafx.scene.control.TabPane

private val log = TabsPanel::class.logger


class TabsPanel (
	val window: MainWindow,
	/**Whenever this window has no tabs (including initially), this type of tab will be created and added.*/
	private val defaultTab: () -> AppTab,
) {
	private val vTabs = TabPane()
	val root: Node = vTabs

	val tabCount: Int get() = vTabs.tabs.size
	val currentIndex: Int get() = vTabs.selectionModel.selectedIndex
	val currentTab: AppTab get() = vTabs.selectionModel.selectedItem.userData as AppTab
	/**Returns 0 if this our first tab, 1 if it's our second, etc., and -1 if this is not a tab in this window.*/
	fun indexOf(tab: AppTab): Int = vTabs.tabs.indexOfFirst { it.userData == tab }
	/**Returns the index of the first tab that matches the condition, or -1 if none matches.*/
	fun indexOf(condition: (AppTab) -> Boolean): Int  = vTabs.tabs.indexOfFirst { condition(it.userData as AppTab) }

	init {
		vTabs.tabs.addListenerAndCallImmediately { changes ->
			while(changes?.next() == true) {
				if (changes.wasRemoved()) {
					for (tab in changes.removed) {
						val appTab = tab.userData as AppTab
						if(appTab.tabsPanel == null)
							log.warn("Tab was already closed: $appTab.") //TODO why does this happen?
						else {
							log.debug("Tab closed: {}.", appTab)
							try {
								appTab.onClose()
								appTab.tabsPanel = null
							} catch (e: Exception) {
								log.error("Failed to close tab $appTab.", e)
								add(appTab, true)
							}
						}
					}
				}
			}
			if (tabCount == 0) {
				log.debug("0 tabs; adding default.")
				add(defaultTab(), true)
			}
		}
	}

	fun add(tab: AppTab, select: Boolean, index: Int = tabCount) {
		if(index < 0 || index > tabCount)
			throw IllegalArgumentException("Invalid add index: $index / $tabCount")
		log.debug("Adding tab at {}: {}.", index, tab)
		val vTab = Tab()
		vTab.textProperty().jfxBind(tab.title)
		vTab.content = tab.content
		vTab.userData = tab
		vTabs.tabs.add(index, vTab)
		tab.tabsPanel = this
		if(select)
			vTabs.selectionModel.select(index)
	}

	fun replace(index: Int, newTab: AppTab) {
		if(index < 0 || index >= tabCount)
			throw IllegalStateException("Invalid replace index: $index / $tabCount")
		log.debug("Replacing tab at {}: {} --> {}.", index, vTabs.tabs[index].userData, newTab)
		val select = vTabs.selectionModel.selectedIndex == index
		add(newTab, select, index)
		val removed = vTabs.tabs.removeAt(index + 1)
		(removed.userData as AppTab).tabsPanel = null
	}

	fun select(index: Int) {
		if(index < 0 || index >= tabCount)
			throw IllegalStateException("Invalid select index: $index / $tabCount")
		log.debug("Selecting tab {}.", index)
		vTabs.selectionModel.select(index)
	}

	fun remove(index: Int) {
		if(index < 0 || index >= tabCount)
			throw IllegalStateException("Invalid remove index: $index / $tabCount")
		log.debug("Removing tab {}.", index)
		vTabs.tabs.removeAt(index)
	}

	fun remove(tab: AppTab) = remove(indexOf(tab))

	fun removeAll() {
		val countToRemove = tabCount
		for(i in 0 until countToRemove)
			remove(0)
	}
}