package imp.org.ui

import imp.jfx.withAction
import imp.org.ui.tabs.DoSomethingTab
import imp.util.logger
import javafx.beans.property.ReadOnlyBooleanProperty
import javafx.beans.property.SimpleBooleanProperty
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Menu
import javafx.scene.control.MenuBar
import javafx.scene.control.MenuItem
import javafx.scene.input.KeyCode
import javafx.scene.input.KeyCodeCombination
import javafx.scene.input.KeyCombination
import javafx.scene.layout.BorderPane
import javafx.stage.Screen
import javafx.stage.Stage


/**Holding this key will cause links to open in new tabs, instead of replacing the current tab.*/
val newTabKey: KeyCode = KeyCode.CONTROL

private val log = MainWindow::class.logger

class MainWindow (
	val stage: Stage
) {
	private val vNewTab = MenuItem("New Tab")
	private val vFile = Menu("File", null, vNewTab)

	private val vMenu = MenuBar(vFile)

	private val tabs = TabsPanel(this) {DoSomethingTab()}
	private val root = BorderPane(tabs.root, vMenu, null, null, null)
	private val scene = Scene(root)

	private val openLinksInNewTabSetter = SimpleBooleanProperty(false)
	/**When true, links should open in new tabs; when false, the existing tab should be replaced.*/
	val openLinksInNewTab: ReadOnlyBooleanProperty = openLinksInNewTabSetter

	init {
		vNewTab.accelerator = KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN)
		vNewTab.withAction(EventHandler { tabs.add(DoSomethingTab(), true) })
		scene.onKeyPressed = EventHandler { event -> if(event.code == newTabKey) openLinksInNewTabSetter.set(true) }
		scene.onKeyReleased = EventHandler { event -> if(event.code == newTabKey) openLinksInNewTabSetter.set(false) }

		val bounds = Screen.getPrimary().bounds
		root.setPrefSize(bounds.width / 2, bounds.height / 2)
		stage.scene = scene
		stage.sizeToScene()
		stage.show()

		scene.window.onCloseRequest = EventHandler {
			log.info("Closing window.")
			tabs.removeAll()
		}
	}
}


