package imp.org.ui.dialogs

import imp.jfx.withAction
import imp.org.OrgLabel
import imp.org.orgExecBackgroundDb
import imp.org.repo.LabelRepo
import imp.util.logger
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.control.Button
import javafx.scene.control.Label
import javafx.scene.layout.HBox
import javafx.scene.layout.VBox
import javafx.stage.Modality
import javafx.stage.Stage

private val log = DeleteLabelDialog::class.logger

class DeleteLabelDialog (
	parentStage: Stage,
	private val label: OrgLabel,
	/**Called in the JavaFX main thread if the user confirms they want to delete the label. Deletion happens later in a background thread.*/
	private val onConfirm: () -> Unit,
) {
	private val stage = Stage()

	init {
		stage.initOwner(parentStage)
		stage.initModality(Modality.WINDOW_MODAL)
		stage.title = "Delete Label?"

		val vTitle = Label("Are you sure you want to delete this label?")
		val vName = Label(label.data.name)
		val vRelations = Label("Implied by ${label.children.size} labels; implies ${label.parents.size} labels.")
		val btnCancel = Button("Cancel").withAction(EventHandler { stage.close() })
		val btnDelete = Button("Delete").withAction(EventHandler { delete() })
		val vButtons = HBox(btnCancel, btnDelete)
		val vContent = VBox(vTitle, vName, vRelations, vButtons)

		stage.scene = Scene(vContent)
		stage.sizeToScene()
		stage.show()
	}

	private fun delete() {
		stage.close()
		onConfirm()
		if(label.data.id == -1L)
			return
		orgExecBackgroundDb { db ->
			try {
				LabelRepo.delete(label, db)
			} catch(e: Exception) {
				log.error("Failed to delete $label.", e)
			}
		}
	}
}