package imp.org.data

import imp.org.AccountD
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.*


/**Caches all accounts in memory.*/
object AccountRepo {
	private val log = this::class.logger
	private val table = orgOrm.table(AccountD::class)

	suspend fun getAll(db: Database): List<AccountD> = ArrayList(loader.get(db).byId.values)
	suspend fun opt(id: Long, db: Database): AccountD? = loader.get(db).byId[id]
	suspend fun req(id: Long, db: Database): AccountD = opt(id, db) ?: throw Exception("Account not found: $id")

	suspend fun save(account: AccountD, db: Database) = loader.save(account, db)
	suspend fun delete(account: AccountD, db: Database) = loader.remove(account, db)


	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface
	interface Listener {
		/**Called after a create or update.*/
		fun postSave(account: AccountD)
		fun postDeleteAccount(id: Long)
	}


	private object loader : SusRepoAllD<AccountD, All, Database>() {
		override suspend fun load(d: Database): All {
			val list = table.query().selectAll(d)
			return All(list.associateBy { it.id })
		}

		override suspend fun with(current: All, item: AccountD, d: Database): All {
			log.debug("Saving account {} {}.", item.id, item.uuid)
			table.save(item, d)
			_listeners.fireAsync { it.postSave(item) }
			return All(current.byId.withEntry(item.id, item))
		}

		override suspend fun without(current: All, item: AccountD, d: Database): All {
			if(item.id == -1L)
				return current
			log.debug("Deleting account {} {}.", item.id, item.uuid)
			table.deletePk(item.id, d)
			_listeners.fireAsync { it.postDeleteAccount(item.id) }
			return All(current.byId.withoutKey(item.id))
		}
	}


	private class All (
		val byId: Map<Long, AccountD>,
	)
}