package imp.org.data

import imp.org.AccountBalanceD
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.BasicCoListeners
import imp.util.logger

private val log = AccountBalanceRepo::class.logger

object AccountBalanceRepo {
	private val table = orgOrm.table(AccountBalanceD::class)

	fun getLast(accountId: Long, db: Database) = table.query().andWhere("accountId = ?").param(accountId).orderBy("timeMs DESC").selectFirst(db)
	/**The list is sorted by time, latest first.*/
	fun getRecent(accountId: Long, count: Int, db: Database) = table.query().andWhere("accountId = ?").param(accountId).orderBy("timeMs DESC").limit(count).selectAll(db)

	fun save(balance: AccountBalanceD, db: Database) {
		log.debug("Saving {}.", balance)
		table.save(balance, db)
		_listeners.fireAsync { it.postSave(balance) }
	}

	fun delete(id: Long, db: Database) {
		log.debug("Deleting account balance {}.", id)
		table.deletePk(id, db)
		_listeners.fireAsync { it.postDeleteBalance(id) }
	}

	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface
	interface Listener {
		/**Called after a create or update.*/
		fun postSave(balance: AccountBalanceD)
		fun postDeleteBalance(id: Long)
	}
}