package imp.org.data

import imp.org.UnitD
import imp.org.orgOrm
import imp.sqlite.Database
import imp.util.*

/**Caches all units in memory.*/
object UnitRepo {
	private val log = this::class.logger
	private val table = orgOrm.table(UnitD::class)

	suspend fun getAll(db: Database): List<UnitD> = loader.get(db).list
	suspend fun opt(id: Long, db: Database): UnitD? = loader.get(db).byId[id]
	suspend fun req(id: Long, db: Database): UnitD = opt(id, db) ?: throw Exception("Unit not found: $id")

	suspend fun save(unit: UnitD, db: Database) = loader.save(unit, db)
	suspend fun delete(unit: UnitD, db: Database) = loader.remove(unit, db)

	suspend fun delete(id: Long, db: Database) {
		val unit = opt(id, db)
		if(unit != null)
			delete(unit, db)
	}


	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface
	interface Listener {
		/**Called after a create or update.*/
		fun postSave(unit: UnitD)
		fun postDeleteUnit(id: Long)
	}


	private object loader : SusRepoAllD<UnitD, All, Database>() {
		override suspend fun load(d: Database): All {
			val list = table.query().selectAll(d)
			return All(list, list.associateBy { it.id })
		}

		override suspend fun with(current: All, item: UnitD, d: Database): All {
			log.debug("Saving unit {} {}.", item.id, item.uuid)
			val new = item.id == -1L
			table.save(item, d)
			val list = if(new) current.list.plus1(item) else current.list
			val byId = current.byId.withEntry(item.id, item)
			_listeners.fireAsync { it.postSave(item) }
			return All(list, byId)
		}

		override suspend fun without(current: All, item: UnitD, d: Database): All {
			if(item.id == -1L)
				return current
			log.debug("Deleting unit {} {}.", item.id, item.uuid)
			table.deletePk(item.id, d)
			val byId = current.byId.withoutKey(item.id)
			val list = ArrayList(byId.values)
			_listeners.fireAsync { it.postDeleteUnit(item.id) }
			return All(list, byId)
		}
	}


	private class All(
		val list: List<UnitD>,
		val byId: Map<Long, UnitD>,
	)
}
