package imp.org.data

import imp.org.NutrientD
import imp.util.bitwiseAnd


/**Describes how the nutrient is measured.*/
enum class NutrientType (
	val flag: Int,
	val longUnitName: String,
) {
	ENERGY(NutrientD.Flags.isEnergy, "calories"),
	MASS(NutrientD.Flags.isMass, "pounds"),
}

fun NutrientD.type(): NutrientType? {
	if(flags.bitwiseAnd(NutrientD.Flags.isEnergy))
		return NutrientType.ENERGY
	if(flags.bitwiseAnd(NutrientD.Flags.isMass))
		return NutrientType.MASS
	return null
}