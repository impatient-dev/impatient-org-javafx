package imp.org.data

import androidx.compose.runtime.Immutable
import imp.org.cui.models.AccountBalanceI
import imp.org.cui.models.AccountI
import imp.org.cui.models.info
import imp.sqlite.Database
import imp.util.fmt.Millis
import java.lang.System.currentTimeMillis

@Immutable data class AccountHistoryI (
	/**Some balances, most recent first.*/
	val entries: List<AccountBalanceI>,
)



/**Returns null for an account that is new (unsaved).*/
suspend fun getAccountRecentHistory(account: AccountI, db: Database): AccountHistoryI? {
	if(!account.isSaved)
		return null
	val now = currentTimeMillis()
	val cutoff = now - 30 * Millis.DAY

	val balances = AccountBalanceRepo.getRecent(account.id, 5, db)
	while(balances.size > 2 && balances.last().timeMs < cutoff)
		balances.removeLast()

	val bi = balances.map { it.info(db) }
	return AccountHistoryI(bi)
}