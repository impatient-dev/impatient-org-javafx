package imp.org.data

import imp.org.NutrientD
import imp.org.orgOrm
import imp.repo.sqlite.MemoryTableRepo
import imp.sqlite.Database
import imp.sqlite.withTransaction
import imp.util.BasicCoListeners
import imp.util.logger

/**Caches all nutrients in memory.*/
object NutrientRepo {
	private val log = this::class.logger
	private val table = orgOrm.table(NutrientD::class)

	suspend fun getAll(db: Database): List<NutrientD> = repo.getAll(db)
	suspend fun opt(id: Long, db: Database): NutrientD? = repo.getById(id, db)
	suspend fun req(id: Long, db: Database): NutrientD = opt(id, db) ?: throw Exception("Nutrient not found: $id")

	suspend fun save(item: NutrientD, db: Database, reorderToEnd: Boolean = false) {
		if(reorderToEnd) {
			val all = repo.getAll(db)
			item.sortOrder = all.size
		}
		repo.save(item, db)
		_listeners.fireAsync { it.postSave(item) }
	}

	suspend fun delete(item: NutrientD, db: Database) {
		repo.delete(item, db)
		_listeners.fireAsync { it.postDelete(item) }
	}

	/**Updates the sort order of the provided nutrients so they will be displayed in the provided order.*/
	suspend fun saveOrder(order: List<NutrientD>, db: Database) {
		var didReorder = false
		db.withTransaction {
			order.forEachIndexed { i, item ->
				assert(item.id != -1L)
				if(item.sortOrder != i) {
					val modified = item.copy(sortOrder = i)
					repo.save(modified, db)
					didReorder = true
				}
			}
		}
		if(didReorder)
			_listeners.fireAsync { it.postReorderNutrients() }
	}


	private val repo = object : MemoryTableRepo<NutrientD>(orgOrm, NutrientD::class) {
		override fun id(record: NutrientD) = record.id
	}


	private val _listeners = BasicCoListeners<Listener>()
	val listeners get() = _listeners.toInterface
	interface Listener {
		suspend fun postSave(item: NutrientD)
		suspend fun postDelete(item: NutrientD)
		suspend fun postReorderNutrients()
	}
}
