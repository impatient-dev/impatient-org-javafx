package imp.org

import imp.sqlite.Database
import imp.util.ReusableNameThreadFactory
import imp.util.logger
import java.util.concurrent.ExecutorService
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

private val threadFactory = ReusableNameThreadFactory("background")
val orgExec: ExecutorService = ThreadPoolExecutor(2, 2, 60, TimeUnit.SECONDS, LinkedBlockingQueue(), threadFactory)

private val log = logger("imp.org.background")

/**Runs a task in a background thread, providing it a connection to the database.*/
fun orgExecBackgroundDb(block: (Database) -> Unit) {
	log.debug("Submitting background task.")
	orgExec.submit {
		try {
			OrgDb.connect().use { db ->
				block(db)
			}
		} catch(e: Throwable) {
			log.error("Error in background thread.", e)
		}
	}
}