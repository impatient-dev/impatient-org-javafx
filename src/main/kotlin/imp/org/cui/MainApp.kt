package imp.org.cui

import androidx.compose.runtime.Composable
import imp.util.logger
import kotlin.system.exitProcess

private val log = MainApp::class.logger

/**The code that runs at the top level of the Compose hierarchy.
 * This class will be responsible for knowing what windows to show and when it's ok to exit.
 * Can only be accessed from the compose thread/dispatcher.*/
class MainApp {
	private val window = MainWindow(this)

	@Composable fun Compose() = CAppTheme { theme ->
		window.Compose(theme)
	}

	fun closeMainWindow() {
		log.info("Exiting")
		exitProcess(0)
	}
}