package imp.org.cui.view

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.rememberCoroutineScope
import imp.compose.cRemember
import imp.compose.rcms
import imp.compose.rcmsn
import imp.compose.update
import imp.org.NutrientD
import imp.org.data.NutrientRepo
import imp.org.orgDbc
import imp.sqlite.cache.use
import imp.util.withDefault
import imp.util.withScope

/**Maintains an up-to-date view of all nutrients, sorted into the user-defined sort order.
 * The argument provided to the lambda will be null until we've loaded the list.*/
@Composable
fun CVNutrients(
	content: @Composable (List<NutrientD>?) -> Unit,
) {
	val scope = rememberCoroutineScope()
	val out = rcmsn<List<NutrientD>>()
	// TODO document, maybe make this reusable
	val loadCtr = rcms(0)
	val reloadCtr = rcms(0)

	NutrientRepo.listeners.cRemember { object : NutrientRepo.Listener {
		override suspend fun postSave(item: NutrientD) {
			reloadCtr.update { it + 1 }
		}

		override suspend fun postDelete(item: NutrientD) {
			reloadCtr.update { it + 1 }
		}

		override suspend fun postReorderNutrients() {
			reloadCtr.update { it + 1 }
		}
	} }

	if(out.value == null || reloadCtr.value != loadCtr.value) {
		val r = reloadCtr.value
		LaunchedEffect(r) {
			withDefault {
				val list = orgDbc.use { db ->
					NutrientRepo.getAll(db)
				}.sortedBy { it.sortOrder }
				withScope(scope) {
					out.value = list
					loadCtr.value = r
				}
			}
		}
	}

	content(out.value)
}