package imp.org.cui

object AppIcons {
	val minus = "icons/material/minus.svg"
	val pencil = "icons/material/pencil.svg"
	val plus = "icons/material/plus.svg"
	val reload = "icons/material/reload.svg"
	val upDown = "icons/material/menu-swap.svg"
}