package imp.org.cui.models

import androidx.compose.runtime.Immutable
import imp.org.fmt
import java.time.Instant
import java.util.*

@Immutable data class UnitI (
	val id: Long,
	val uuid: UUID,
	val name: String,
	val short: String?,
	val symbol: String?,
	val description: String?,
	val fractionalDigits: Int,
) {
	val isSaved get() = id != -1L
}

@Immutable data class AccountI (
	val id: Long,
	val uuid: UUID,
	val name: String,
	val description: String?,
	val unit: UnitI,
) {
	val isSaved get() = id != -1L
}

@Immutable data class AccountBalanceI (
	val id: Long,
	val uuid: UUID,
	val account: AccountI,
	val time: Instant,
	val amount: Long,
	val title: String?,
	val description: String?,
) {
	val isSaved get() = id != -1L
	inline val unit get() = account.unit

	fun fmt() = unit.fmt(amount)

	companion object {
		fun unsaved(account: AccountI, time: Instant = Instant.now()) = AccountBalanceI(-1, UUID.randomUUID(), account, time, 0, null, null)
	}
}