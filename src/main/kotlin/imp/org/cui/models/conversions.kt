package imp.org.cui.models

import imp.org.AccountBalanceD
import imp.org.AccountD
import imp.org.UnitD
import imp.org.data.AccountRepo
import imp.org.data.UnitRepo
import imp.sqlite.Database
import java.time.Instant


fun UnitI.data() = UnitD(id, uuid, name, short, symbol, description, fractionalDigits)
fun UnitD.info() = UnitI(id, uuid, name, short, symbol, description, fractionalDigits)

suspend fun AccountD.info(db: Database): AccountI {
	val unit = UnitRepo.req(unitId, db).info()
	return AccountI(id, uuid, name, description, unit)
}
fun AccountI.data() = AccountD(id, uuid, name, description, unit.id)

suspend fun AccountBalanceD.info(db: Database): AccountBalanceI {
	val account = AccountRepo.req(accountId, db).info(db)
	return AccountBalanceI(id, uuid, account, Instant.ofEpochMilli(timeMs), amount, title, description)
}
fun AccountBalanceI.data() = AccountBalanceD(id, uuid, account.id, amount, time.toEpochMilli(), title, description)