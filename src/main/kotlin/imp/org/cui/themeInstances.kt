package imp.org.cui

import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Shapes
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.unit.dp
import imp.compose.ColorPair
import imp.compose.opaque
import imp.compose.under


private val shapes = Shapes(
	small = RoundedCornerShape(4.dp),
	medium = RoundedCornerShape(4.dp),
	large = RoundedCornerShape(0.dp)
)


private val c1 = opaque(0x78909c) // blue-grey 400
private val c1Dark = opaque(0x4b636e)
private val c1Lite = opaque(0xa7c0cd)

private val lightTheme = AppTheme(
	main = Color.White.under(),
	primary = c1,
	primaryStrong = c1Dark,
	primaryWeak = c1Lite,
	foreDisabled = opaque(0xaaaaaa),
	foreError = Color.Red,
	disabled = ColorPair(opaque(0xd6dfdf), opaque(0x668888)),
)


/**Applies the material theme, and returns the current app theme object.*/
@Composable fun CAppTheme(content: @Composable (AppTheme) -> Unit) {
	val theme = lightTheme
	MaterialTheme(
		colors = theme.material,
		typography = appTypography,
		shapes = shapes,
	) {
		content(theme)
	}
}


interface AppComposition {
	@Composable fun Compose(theme: AppTheme)
}