package imp.org.cui

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.window.Window
import imp.compose.*
import imp.org.cui.panel.AccountListPanel
import imp.org.cui.panel.NutrientListPanel
import imp.org.cui.panel.UnitListPanel

/**May only be accessed from the compose thread/dispatcher.*/
class MainWindow (
	private val app: MainApp,
) {
	private val left = cms(defaultPanel())
	private val right = cmsn<Panel>()

	@Composable fun Compose(theme: AppTheme) = Window(onCloseRequest = { app.closeMainWindow() }, title = "impatient-org") {
		CSurface {
			Column(Modifier.fillMaxSize()) {
				CRowWC { CLeftPicker(theme) }
				CWeightRowC {
					CWeightBox { left.value.Compose(theme) }
					theme.divider.CV()
					CWeightBox { right.value?.Compose(theme) }
				}
			}
		}
	}

	/**A horizontal picker that lets the user open major/common panels on the left.*/
	@Composable private fun CLeftPicker(theme: AppTheme) {
		val p = left.value
		theme.toggle.COption("Units", p is UnitListPanel) { open(UnitListPanel(this)) }
		theme.toggle.COption("Accounts", p is AccountListPanel) { open(AccountListPanel(this)) }
		theme.toggle.COption("Nutrients", p is NutrientListPanel) { open(NutrientListPanel(this)) }
	}

	/**Lets the user open a search panel in the left tab.*/
	fun open(panel: Panel) {
		if(belongsOnleft(panel))
			left.value = panel
		else
			right.value = panel
	}

	fun close(panel: Panel) {
		if(panel === right.value)
			right.value = null
		else if(panel === left.value)
			left.value = defaultPanel()
	}

	private fun defaultPanel(): Panel = UnitListPanel(this)

	interface Panel {
		@Composable fun Compose(theme: AppTheme)
	}

	private fun belongsOnleft(panel: Panel): Boolean = when(panel) {
		is AccountListPanel -> true
		is NutrientListPanel -> true
		is UnitListPanel -> true
		else -> false
	}
}