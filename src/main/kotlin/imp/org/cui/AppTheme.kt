package imp.org.cui

import androidx.compose.material.Colors
import androidx.compose.material.Typography
import androidx.compose.runtime.Immutable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.font.FontFamily
import androidx.compose.ui.text.font.FontStyle
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import imp.compose.ColorPair
import imp.compose.maxContrast
import imp.compose.style.*
import imp.compose.under
import imp.util.desktop.csIcon

private val family = FontFamily.SansSerif
val appTypography = Typography(body1 = csText(family, Color.Unspecified, 14.sp).style)


/**Appropriate horizontal padding between separate pieces of body text.*/
val hpadBody = 4.dp
/**A large vertical separation.*/
val vpadBig = 12.dp


@Immutable class AppTheme (
	main: ColorPair,
	val primary: Color,
	primaryStrong: Color,
	primaryWeak: Color,
	foreDisabled: Color,
	foreError: Color,
	disabled: ColorPair,
) {
	val body = appTypography.body1.csText.copy(color = main.fore)

	val label = body.copy(color = foreDisabled, fontStyle = FontStyle.Italic)

	val strong = body.copy(weight = FontWeight.Bold)
	val strongError = strong.copy(color = foreError)

	val bigMonoPrimary = body.copy(family = FontFamily.Monospace, size = 24.sp, color = primary)

	val panelHead = body.copy(size = 24.sp, weight = FontWeight.W600)

	val link = body.csLink(foreDisabled, hpad = 4.dp)
	val bigLink = link.copy(size = 20.sp)

	val btn = csBtnText(family, 16.sp, FontWeight.W500, primary.under(), disabled).csIcon()

	val toggle = CSToggle(
		primaryStrong.under().csText(body.family, 16.sp, FontWeight.W600),
		primaryWeak.under().csText(body.family, 16.sp, FontWeight.W600),
		10.dp,
	)

	val divider = CSDivider(main.fore, 2.dp)
	val dragTo = divider

	val material = Colors(
		primary = primary,
		primaryVariant = primary,
		secondary = primary,
		secondaryVariant = primary,
		background = main.back,
		surface = main.back,
		error = foreError,
		onPrimary = primary.maxContrast(),
		onSecondary = primary.maxContrast(),
		onBackground = main.fore,
		onSurface = main.fore,
		onError = foreError.maxContrast(),
		isLight = main.fore.maxContrast() == Color.White,
	)
}

