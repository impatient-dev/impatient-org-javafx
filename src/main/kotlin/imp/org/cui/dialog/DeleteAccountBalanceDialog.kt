package imp.org.cui.dialog

import androidx.compose.runtime.Composable
import imp.compose.CColMaxScrollV
import imp.compose.CRowC
import imp.org.OrgDb
import imp.org.backScope
import imp.org.cui.AppComposition
import imp.org.cui.AppTheme
import imp.org.cui.models.AccountBalanceI
import imp.org.data.AccountBalanceRepo
import imp.org.fmt
import imp.util.desktop.CDialog
import imp.util.launchIo
import imp.util.toLocal

/**Asks for confirmation before deleting.*/
class DeleteAccountBalanceDialog (
	private val balance: AccountBalanceI,
	private val close: () -> Unit,
) : AppComposition {
	@Composable override fun Compose(theme: AppTheme) = CDialog("Delete Balance", close) {
		CColMaxScrollV {
			theme.strong.C("Delete this balance?")
			theme.body.C(balance.unit.fmt(balance.amount))
			theme.body.C(balance.time.toLocal().toString())

			CRowC {
				theme.link.C("Cancel", onClick = close)
				theme.btn.C("Delete") {
					backScope.launchIo {
						OrgDb.connectSus().use { db -> AccountBalanceRepo.delete(balance.id, db) }
					}
					close()
				}
			}
		}
	}
}