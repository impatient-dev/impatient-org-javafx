package imp.org.cui.dialog

import androidx.compose.material.DropdownMenu
import androidx.compose.material.DropdownMenuItem
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import imp.compose.*
import imp.org.AccountD
import imp.org.OrgDb
import imp.org.backScope
import imp.org.cui.AppComposition
import imp.org.cui.AppTheme
import imp.org.cui.hpadBody
import imp.org.cui.models.AccountI
import imp.org.cui.models.UnitI
import imp.org.cui.models.info
import imp.org.data.AccountRepo
import imp.org.data.UnitRepo
import imp.util.blankToNull
import imp.util.desktop.CDialog
import imp.util.withDefault
import kotlinx.coroutines.launch

/**Lets the user edit basic details (name, description).*/
class EditAccountDialog (
	val account: AccountI,
	val close: () -> Unit,
) : AppComposition {
	private val name = cms(account.name)
	private val description = cms(account.description ?: "")
	private val unit = cmsn<UnitI>()
	private val allUnits = cmsn<List<UnitI>>()

	private inline val createOrEditStr get() = if(account.isSaved) "Edit" else "Create"

	@Composable override fun Compose(theme: AppTheme) = CDialog("$createOrEditStr Account", close) {
		val us = allUnits.value
		val scope = rememberCoroutineScope()
		val critical = remember { CCritical(scope) }

		if(us == null) {
			LaunchedEffect(null) { critical.launchIf { loadUnits() } }
			CSpinnerFull()
		} else {
			CColMaxScrollV {
				CContent(us, theme)
			}
		}
	}

	@Composable private fun CContent(
		units: List<UnitI>,
		theme: AppTheme,
	) {
		val n = name.value
		val d = description.value
		val u = unit.value

		theme.panelHead.C("Unit")
		CTextField(n, "Name", "My Bank", isError = n.isBlank()) { name.value = it }
		CTextField(d, "Description", "", singleLine = false) { description.value = it }
		CAccountUnit(u, units, account, theme)

		CRowWR {
			theme.link.C("Cancel") { close() }
			theme.btn.C("Save", enabled = !n.isBlank() && u != null) { save() }
		}
	}

	@Composable private fun CAccountUnit(
		unit: UnitI?,
		all: List<UnitI>,
		acct: AccountI,
		theme: AppTheme,
	) {
		CRowC {
			theme.label.C("currency")
			CHPad(hpadBody)
			if(unit != null)
				theme.body.C(unit.name)
			else
				theme.strongError.C("Missing")
			if(!acct.isSaved) {
				val open = rcms(false)
				theme.btn.C("...") { open.value = true }
				DropdownMenu(open.value, { open.value = false }) {
					for(u in all)
						DropdownMenuItem({ this@EditAccountDialog.unit.value = u }) { theme.body.C(u.name) }
				}
			}
		}
	}


	private suspend fun loadUnits() {
		val units = withDefault {
			OrgDb.connectSus().use { db ->
				UnitRepo.getAll(db)
					.map { it.info() }
					.sortedBy { it.name }
			}
		}
		allUnits.value = units
	}

	private fun save() {
		val data = AccountD(account.id, account.uuid, name.value, description.value.blankToNull(), unit.value!!.id)
		backScope.launch {
			OrgDb.connectSus().use { db -> AccountRepo.save(data, db) }
		}
		close()
	}
}