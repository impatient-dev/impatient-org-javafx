package imp.org.cui.dialog

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.unit.DpSize
import androidx.compose.ui.unit.dp
import imp.compose.*
import imp.org.OrgDb
import imp.org.backScope
import imp.org.cui.AppComposition
import imp.org.cui.AppTheme
import imp.org.cui.models.AccountBalanceI
import imp.org.cui.models.data
import imp.org.data.AccountBalanceRepo
import imp.org.fmtN
import imp.util.*
import imp.util.desktop.CDialog
import imp.util.fmt.parseDateTime
import kotlinx.coroutines.launch
import java.lang.System.currentTimeMillis
import java.math.BigDecimal
import java.time.LocalDateTime

// TODO detect when balance is deleted and close automatically
/**Dialog that lets the user record a new balance for an account, or update an existing one.*/
class RecordBalanceDialog (
	private val balance: AccountBalanceI,
	private val close: () -> Unit,
) : AppComposition {
	private val delegate = cmsn<AppComposition>()
	private inline val new get() = balance.id == -1L
	private inline val unit get() = balance.account.unit


	@Composable override fun Compose(theme: AppTheme) = CDialog("Account Balance", close, DpSize(400.dp, 600.dp)) {
		CColMaxScrollV {
			val parseAmt = remember { CLookupSync(if(new) "" else unit.fmtN(balance.amount), doLookup = ::parseAmount) }
			val parseWhen = remember { CLookupSync("", doLookup = ::parseWhen) }
			val title = rcms("")
			val description = rcms("")

			delegate.value?.Compose(theme)
			theme.body.C("Record new balance for account:")
			theme.strong.C(balance.account.name)

			// amount
			CRowC {
				if(unit.symbol != null)
					theme.strong.C(unit.symbol!!)
				CTextField(parseAmt.input ?: "", "Amount", "", isError = parseAmt.output == null) { parseAmt.submit(it) }
				if(balance.account.unit.symbol == null)
					theme.strong.C(unit.short ?: unit.name)
			}

			// when
			CRowC {
				CTextField(parseWhen.input ?: "", "Date/Time", "Tuesday", isError = parseWhen.input?.isEmpty() != true && parseWhen.output == null) { parseWhen.submit(it) }
				if(!new && parseWhen.input?.isEmpty() == true)
					theme.strong.C(balance.time.toLocal().toString())
			}

			CTextField(title.value, "Title", "") { title.value = it }
			CTextField(description.value, "Description", "", singleLine = false) { description.value = it }

 			//buttons
			CRowC {
				theme.link.C("Cancel", onClick = close)
				if(!new)
					theme.btn.C("Delete") { delegate.value = DeleteAccountBalanceDialog(balance) { delegate.value = null } }
				theme.btn.C("Save", enabled = parseAmt.output != null && (!new || parseWhen.input?.isEmpty() == true || parseWhen.output != null)) {
					val record = balance.data().copy(
						title = title.value.blankToNull(),
						description = description.value.blankToNull(),
						amount = parseAmt.output!!,
						timeMs = parseWhen.output?.toInstant()?.toEpochMilli() ?: currentTimeMillis(),
					)
					backScope.launch {
						OrgDb.connectSus().use { db -> AccountBalanceRepo.save(record, db) }
					}
					close()
				}
			}
		}
	}


	/**Returns null if the input isn't a valid balance for this account.*/
	private fun parseAmount(str: String): Long? = exToNull {
		var d = BigDecimal(str)
		val f = unit.fractionalDigits
		if(f > 0)
			d *= BigDecimal.valueOf(pow(10L, f.toLong()))
		else if(f < 0)
			d /= BigDecimal.valueOf(pow(10, -f.toLong()))
		return d.longValueExact()
	}

	private fun parseWhen(str: String): LocalDateTime? = exToNull {
		if(str.isEmpty()) return null
		return parseDateTime(str, future = false)
	}
}
