package imp.org.cui.panel

import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import imp.compose.*
import imp.org.NutrientD
import imp.org.cui.AppTheme
import imp.org.cui.MainWindow
import imp.org.cui.vpadBig
import imp.org.data.NutrientRepo
import imp.org.data.NutrientType
import imp.org.data.type
import imp.org.orgDbc
import imp.sqlite.cache.use
import imp.util.bitsOnlyIf
import imp.util.bitwiseAnd
import imp.util.blankToNull
import imp.util.ioScope
import kotlinx.coroutines.launch

/**Lets the user edit a new or existing nutrient.*/
class NutrientDetailPanel (
	private val item: NutrientD,
	private val window: MainWindow,
) : MainWindow.Panel {
	private val isNew = item.id == -1L
	private val name = cms(item.name)
	private val desc = cms(item.desc ?: "")
	private val type = cms(item.type() ?: NutrientType.ENERGY)
	private val showOnlyUnit = cms(item.flags.bitwiseAnd(NutrientD.Flags.showOnlyUnit))

	@Composable
	override fun Compose(theme: AppTheme) = CColMaxScrollV(horizontal = Alignment.Start) {
		theme.panelHead.C((if(isNew) "New " else "") + "Nutrient")

		val n = name.value
		val d = desc.value
		CTextField(n, "Name", "", isError = n.isBlank()) { name.value = it }
		CTextField(d, "Description", "", singleLine = false) { desc.value = it }

		CTypes(theme)

		CCheckWCol(showOnlyUnit.value, { showOnlyUnit.value = it }) {
			theme.strong.C("Unit Is Self-Explanatory")
			theme.body.C("If you check this, some screens will just show the unit (e.g. calories) without the nutrient name.")
		}

		CRowWR {
			theme.btn.C("Save", enabled = !n.isBlank(), onClick = ::save)
		}
	}


	@Composable private fun CTypes(theme: AppTheme) = CColWC {
		val t = type.value
		CRowL {
			theme.body.CRadio("Energy (calories)", t == NutrientType.ENERGY, enabled = isNew) { type.value = NutrientType.ENERGY }
			theme.body.CRadio("Mass (lb)", t == NutrientType.MASS, enabled = isNew) { type.value = NutrientType.MASS }
		}
		if(!isNew) {
			theme.label.C("The type can only be changed for a new nutrient.")
			CVPad(vpadBig)
		}
	}


	private fun save() {
		val data = item.copy(
			name = name.value,
			desc = desc.value.blankToNull(),
			flags = type.value.flag or NutrientD.Flags.showOnlyUnit.bitsOnlyIf(showOnlyUnit.value),
		)
		ioScope().launch {
			orgDbc.use { db ->
				NutrientRepo.save(data, db, reorderToEnd = data.id == -1L)
			}
		}
		window.close(this)
	}
}
