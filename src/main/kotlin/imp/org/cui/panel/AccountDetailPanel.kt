package imp.org.cui.panel

import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Alignment
import imp.compose.*
import imp.org.OrgDb
import imp.org.cui.AppComposition
import imp.org.cui.AppIcons
import imp.org.cui.AppTheme
import imp.org.cui.MainWindow
import imp.org.cui.dialog.RecordBalanceDialog
import imp.org.cui.models.AccountBalanceI
import imp.org.cui.models.AccountI
import imp.org.cui.models.info
import imp.org.data.AccountHistoryI
import imp.org.data.AccountRepo
import imp.org.data.getAccountRecentHistory
import imp.org.fmtFromNow
import imp.util.toLocal
import imp.util.withDefault
import imp.util.withScope
import kotlinx.coroutines.CoroutineScope
import java.time.LocalDateTime
import java.time.ZoneId

class AccountDetailPanel (
	private val id: Long,
	private val window: MainWindow,
) : MainWindow.Panel {
	private val account = cmsn<AccountI>()
	private val history = cmsn<AccountHistoryI>()

	@Composable override fun Compose(theme: AppTheme) = CColMaxScrollV(horizontal = Alignment.Start) {
		val scope = rememberCoroutineScope()
		val critical = remember(this) { CCritical(scope) }
		val acct = account.value

		if(acct == null) {
			LaunchedEffect(this) { critical.launchForce { reload(scope) }}
			CSpinnerFull()
		} else {
				CAccountBasics(acct, critical,  theme)
				CAccountHistory(acct, history.value, critical.busy, theme)
		}
	}


	@Composable private fun CAccountBasics(acct: AccountI, critical: CCritical, theme: AppTheme) {
		theme.panelHead.C("Account")
		theme.strong.C(acct.name)
		if(acct.description != null)
			theme.body.C(acct.description)
		theme.strong.C(acct.unit.name)

		CRowWR {
			theme.btn.CIcon(AppIcons.reload) { critical.launchIf { scope -> reload(scope) }}
		}
	}

	@Composable fun CAccountHistory(
		account: AccountI,
		history: AccountHistoryI?,
		busy: Boolean,
		theme: AppTheme,
	) {
		val dialog = rcmsn<AppComposition>()
		dialog.value?.Compose(theme)
		if(account.isSaved)
			theme.btn.C("Record Balance", enabled = !busy) { dialog.value = RecordBalanceDialog(AccountBalanceI.unsaved(account)) { dialog.value = null } }

		if(history != null) {
			val now = LocalDateTime.now()
			val zone = ZoneId.systemDefault()
			for(b in history.entries) {
				CRowC {
					theme.body.C("${b.fmt()} ${fmtFromNow(b.time.toLocal(zone), now)}")
					theme.btn.CIcon(AppIcons.pencil) { dialog.value = RecordBalanceDialog(b) { dialog.value = null } }
				}
			}
		}
	}


	private suspend fun reload(scope: CoroutineScope) {
		withDefault {
			val a: AccountI
			val h: AccountHistoryI
			OrgDb.connectSus().use { db ->
				a = AccountRepo.req(id, db).info(db)
				h = getAccountRecentHistory(a, db)!!
			}
			withScope(scope) {
				account.value = a
				history.value = h
			}
		}
	}
}