package imp.org.cui.panel

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import imp.compose.*
import imp.org.AccountD
import imp.org.OrgDb
import imp.org.cui.AppComposition
import imp.org.cui.AppIcons
import imp.org.cui.AppTheme
import imp.org.cui.MainWindow
import imp.org.data.AccountRepo
import kotlinx.coroutines.Dispatchers

class AccountListPanel (
	private val window: MainWindow,
) : MainWindow.Panel {

	@Composable override fun Compose(theme: AppTheme) = Column {
		val scope = rememberCoroutineScope()
		val loader = remember { CLookupAsync(scope, Dispatchers.IO, Unit, deduplicate = false) { loadList() } }
		AccountRepo.listeners.cRemember(scope) { MyListener { loader.submit(Unit) } }
		val list = loader.output ?: emptyList()

		val dialog = rcmsn<AppComposition>()
		dialog.value?.Compose(theme)

		theme.panelHead.C("Accounts (${list.size})")
		CWeightBox {
			Column(Modifier.fillMaxSize().verticalScroll(rememberScrollState())) {
				list.forEachIndexed { i, account ->
					theme.bigLink.CW(account.name) { window.open(AccountDetailPanel(account.id, window)) }
				}
			}
		}
		CRowWR {
			if(loader.isWorking)
				CSpinner()
			theme.btn.CIcon(AppIcons.reload, enabled = !loader.isWorking) { loader.submit(Unit) }
			theme.btn.CIcon(AppIcons.plus) { window.open(AccountDetailPanel(-1, window)) }
		}
	}

	private suspend fun loadList(): List<AccountD> {
		OrgDb.connectSus().use { db ->
			return AccountRepo.getAll(db)
				.sortedBy { it.name }
		}
	}

	private class MyListener(private val reload: () -> Unit) : AccountRepo.Listener {
		override fun postSave(account: AccountD) = reload()
		override fun postDeleteAccount(id: Long) = reload()
	}
}