package imp.org.cui.panel

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import imp.compose.*
import imp.org.OrgDb
import imp.org.UnitD
import imp.org.cui.AppIcons
import imp.org.cui.AppTheme
import imp.org.cui.MainWindow
import imp.org.data.UnitRepo
import imp.org.fmtFracD
import imp.util.blankAndNotEmpty
import imp.util.blankToNull
import imp.util.ioScope
import imp.util.logger
import kotlinx.coroutines.launch
import java.lang.Integer.max

private val log = UnitDetailPanel::class.logger

class UnitDetailPanel (
	unit: UnitD,
	private val window: MainWindow,
) : MainWindow.Panel {
	private val id = cms(unit.id)
	private val uuid = cms(unit.uuid)
	private val name = cms(unit.name)
	private val short = cms(unit.short ?: "")
	private val symbol = cms(unit.symbol ?: "")
	private val description = cms(unit.description ?: "")
	private val fractionalDigits = cms(unit.fractionalDigits)

	@Composable override fun Compose(theme: AppTheme) = CColMaxScrollV(horizontal = Alignment.Start) {
		val n = name.value
		val sn = short.value
		val sy = symbol.value
		val f = fractionalDigits.value
		val new = id.value == -1L
		theme.panelHead.C("Unit")
		CTextField(n, "Name", "Dollars", isError = n.isBlank()) { name.value = it }
		CTextField(sn, "Short Name", "USD", isError = sn.blankAndNotEmpty()) { short.value = it }
		CTextField(sy, "Symbol", "$", isError = sy.blankAndNotEmpty()) { symbol.value = it }
		CTextField(description.value, "Description", "", singleLine = false) { description.value = it }

		Row(verticalAlignment = Alignment.CenterVertically) {
			theme.body.C("Fractional Digits")
			theme.bigMonoPrimary.C("${fractionalDigits.value}")
			Column {
				theme.btn.CIconPlain(AppIcons.plus, enabled = new && f < 8) { fractionalDigits.update { it + 1 } }
				theme.btn.CIconPlain(AppIcons.minus, enabled = new && f > -8) { fractionalDigits.update { it - 1 } }
			}
		}
		if(!new)
			theme.body.C("Cannot be changed for an existing unit.")

		val example = exampleNumberWithDigits(1 + max(2, f))
		theme.body.C("${fmtFracD(example, f)} $n")
		if(sn.isNotBlank())
			theme.body.C("${fmtFracD(example, f)} $sn")
		if(sy.isNotBlank())
			theme.body.C("$sy${fmtFracD(example, f)}")

		CRowWR {
			theme.btn.C("Delete", onClick = ::delete)
			theme.btn.C("Save", enabled = !n.isBlank() && !sn.blankAndNotEmpty() && !sy.blankAndNotEmpty(), onClick = ::save)
		}
	}


	private fun delete() {
		val id = id.value
		if(id != -1L)
			ioScope().launch {
				OrgDb.connectSus().use { db -> UnitRepo.delete(id, db) }
			}
		window.close(this)
	}

	private fun save() {
		val data = UnitD(
			id = id.value,
			uuid = uuid.value,
			name = name.value,
			short = short.value.blankToNull(),
			symbol = symbol.value.blankToNull(),
			description = description.value.blankToNull(),
			fractionalDigits.value,
		)
		ioScope().launch {
			OrgDb.connectSus().use { db -> UnitRepo.save(data, db) }
		}
		window.close(this)
	}

	private fun exampleNumberWithDigits(digits: Int): Long {
		var out = 1L
		for(i in 2 .. digits)
			out = (10 * out) + (i % 10)
		return out
	}
}