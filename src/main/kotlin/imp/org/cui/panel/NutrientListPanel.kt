package imp.org.cui.panel

import androidx.compose.foundation.gestures.Orientation
import androidx.compose.foundation.gestures.draggable
import androidx.compose.foundation.gestures.rememberDraggableState
import androidx.compose.foundation.layout.Column
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.onGloballyPositioned
import imp.compose.*
import imp.org.NutrientD
import imp.org.backScope
import imp.org.cui.AppIcons
import imp.org.cui.AppTheme
import imp.org.cui.MainWindow
import imp.org.cui.view.CVNutrients
import imp.org.data.NutrientRepo
import imp.org.data.type
import imp.org.orgDbc
import imp.sqlite.cache.use
import imp.util.bitwiseAnd
import imp.util.desktop.CIcon
import imp.util.move
import kotlinx.coroutines.launch
import java.util.*

class NutrientListPanel (
	private val window: MainWindow,
) : MainWindow.Panel {

	@Composable override fun Compose(theme: AppTheme) = Column {
		CVNutrients { list ->
			theme.panelHead.C("Nutrients")
			CWeightBox {
				if(list == null) {
					CSpinnerHC()
				} else {
					CContent(list, theme)
				}
			}
		}
	}

	@Composable private fun CContent(
		list: List<NutrientD>,
		theme: AppTheme,
	) = CColMaxScrollV {
		CHR {
			theme.btn.C("New...") { window.open(NutrientDetailPanel(newUnsaved(), window)) }
		}

		val heightsByIdx = remember { HashMap<Int, Int>() }
		val draggingToBeforeIndex = rcms(-1)

		list.forEachIndexed { i, nutrient ->
			if(draggingToBeforeIndex.value == i)
				theme.dragTo.CH()
			CRowWC(Modifier.onGloballyPositioned { heightsByIdx[i] = it.size.height }) {
				val deltaV = rcms(0f)
				val dragState = rememberDraggableState { delta -> deltaV.update { it + delta } }

				val displayName = if(nutrient.flags.bitwiseAnd(NutrientD.Flags.showOnlyUnit)) nutrient.type()!!.longUnitName else nutrient.name
				theme.bigLink.C(displayName) { window.open(NutrientDetailPanel(nutrient, window)) }
				CWeight()
				theme.primary.CIcon(AppIcons.upDown, Modifier.draggable(dragState, Orientation.Vertical,
					onDragStarted = {},
					onDragStopped = {
						val order = ArrayList(list)
						order.move(i, draggingToBeforeIndex.value)
						draggingToBeforeIndex.value = -1
						deltaV.value = 0f
						backScope.launch { orgDbc.use { db -> NutrientRepo.saveOrder(order, db) } }
					},
				))

				val dv = deltaV.value
				if(dv != 0f)
					LaunchedEffect(dv) {
						draggingToBeforeIndex.value = calculateDragToIndex(i, dv, heightsByIdx)
					}
			}
		}
		if(draggingToBeforeIndex.value == list.lastIndex + 1)
			theme.dragTo.CH()
	}


	private fun newUnsaved() = NutrientD(
		id = -1,
		uuid = UUID.randomUUID(),
		flags = 0,
		name = "",
		desc = null,
		sortOrder = 0,
	)

	/**Returns -1 if it can't give a good answer.*/
	private fun calculateDragToIndex(fromIdx: Int, dragAmt: Float, heightsByIdx: Map<Int, Int>): Int {
		// TODO this falsely assumes the drag started in the center of the dragged element
		// TODO Map<Int, Int> is inefficient compared with a list
		val fromHeight = heightsByIdx[fromIdx] ?: return -1
		if(dragAmt > 0) { // down
			var distanceRemaining = dragAmt - fromHeight / 2
			var idx = fromIdx + 1
			while(true) {
				var nextHeight = heightsByIdx[idx]
				if(nextHeight == null || distanceRemaining < nextHeight / 2)
					return idx
				distanceRemaining -= nextHeight
				idx++
			}
		} else { // up
			var distanceRemaining = -dragAmt - fromHeight / 2
			var idx = fromIdx - 1
			while(true) {
				if(idx == 0)
					return 0
				var nextHeight = heightsByIdx[idx]
				if(nextHeight == null || distanceRemaining < nextHeight / 2)
					return idx
				distanceRemaining -= nextHeight
				idx--
			}
		}
	}
}