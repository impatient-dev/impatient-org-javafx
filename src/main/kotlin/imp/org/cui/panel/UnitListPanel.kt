package imp.org.cui.panel

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import imp.compose.*
import imp.org.OrgDb
import imp.org.UnitD
import imp.org.cui.AppIcons
import imp.org.cui.AppTheme
import imp.org.cui.MainWindow
import imp.org.data.UnitRepo
import kotlinx.coroutines.Dispatchers
import java.util.*

class UnitListPanel (
	private val window: MainWindow,
) : MainWindow.Panel {

	@Composable override fun Compose(theme: AppTheme) = Column {
		val scope = rememberCoroutineScope()
		val loader = remember { CLookupAsync(scope, Dispatchers.IO, Unit, deduplicate = false) { loadList() } }
		UnitRepo.listeners.cRemember(scope) { MyListener { loader.submit(Unit) } }
		val list = loader.output ?: emptyList()

		theme.panelHead.C("Units (${list.size})")
		CWeightBox {
			Column(Modifier.fillMaxSize().verticalScroll(rememberScrollState())) {
				list.forEachIndexed { i, unit ->
					theme.bigLink.CW(unit.name) { window.open(UnitDetailPanel(unit, window)) }
				}
			}
		}
		CRowWR {
			if(loader.isWorking)
				CSpinner()
			theme.btn.CIcon(AppIcons.reload, enabled = !loader.isWorking) { loader.submit(Unit) }
			theme.btn.CIcon(AppIcons.plus) { window.open(UnitDetailPanel(newUnsaved(), window)) }
		}
	}

	private fun newUnsaved() = UnitD(-1, UUID.randomUUID(), "", null, null, null, 0)

	private suspend fun loadList(): List<UnitD> {
		val all = OrgDb.connectSus().use { db -> UnitRepo.getAll(db) }
			.sortedBy { it.name }
		return all
	}

	private class MyListener(private val reload: () -> Unit) : UnitRepo.Listener {
		override fun postSave(unit: UnitD) = reload()
		override fun postDeleteUnit(id: Long) = reload()
	}
}