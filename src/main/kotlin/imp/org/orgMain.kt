package imp.org

import androidx.compose.ui.window.application
import com.sun.javafx.application.LauncherImpl
import imp.logback.initLogbackCreateConfigIfMissing
import imp.org.cui.MainApp
import imp.org.ui.MainWindow
import imp.util.assertWorkingDirContains
import imp.util.defaultScope
import imp.util.logger
import javafx.application.Application
import javafx.application.Preloader
import javafx.stage.Stage
import java.nio.file.Path

/**General scope for doing background things. This scope is never cancelled.*/
val backScope = defaultScope()

fun main() {
	assertWorkingDirContains("src/main/kotlin", "impatient-org.jar")
	initLogbackCreateConfigIfMissing(Path.of("logback.xml"), "/logback-default.xml", OrgJfxApp::class)
	if(true) {
		val app = MainApp()
		application { app.Compose() }
	} else {
		LauncherImpl.launchApplication(OrgJfxApp::class.java, OrgJfxPreloader::class.java, emptyArray())
	}
}

private val appTitle = "impatient-org"


/**Sets the application name that appears in the Gnome dock. Otherwise, it's a class name. See stackoverflow.com/a/54467323.*/
class OrgJfxPreloader : Preloader() {
	override fun start(primaryStage: Stage) {
		com.sun.glass.ui.Application.GetApplication().name = appTitle
	}
}


class OrgJfxApp : Application() {
	private val log = OrgJfxApp::class.logger

	override fun start(primaryStage: Stage) {
		log.debug("Creating UI.")
		primaryStage.title = appTitle
		MainWindow(primaryStage)
		log.debug("UI setup finished.")
	}
}